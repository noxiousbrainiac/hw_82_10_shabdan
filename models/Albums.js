const mongoose = require('mongoose');
const idValidator = require('mongoose-id-validator');

const AlbumsSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    artist_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Artists',
        required: true
    },
    productionYear: {
        type: String,
        required: true,
    },
    image: String
});

AlbumsSchema.plugin(idValidator);
const Albums = mongoose.model('Albums', AlbumsSchema);
module.exports = Albums;