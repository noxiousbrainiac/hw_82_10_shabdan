const mongoose = require('mongoose');
const idValidator = require('mongoose-id-validator');

const TracksSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    album_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Albums',
        required: true
    },
    duration: {
        type: String,
        required: true
    }
});

TracksSchema.plugin(idValidator);
const Tracks = mongoose.model('Tracks', TracksSchema);
module.exports = Tracks;