const mongoose = require('mongoose');

const ArtistsSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    image: String,
    info: String
});

const Artists = mongoose.model('Artists', ArtistsSchema)
module.exports = Artists;