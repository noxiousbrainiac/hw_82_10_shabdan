const express = require('express');
const multer = require('multer');
const path = require('path');
const config = require('../config');
const Albums = require('../models/Albums');
const {nanoid} = require("nanoid");

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const router = express.Router();

router.get('/', async (req, res) => {
    try {
        if (req.query.artist) {
            const album = await Albums.find({artist_id: req.query.artist});
            return res.send(album);
        }

        const albums = await Albums.find();
        res.send(albums);
    } catch (e) {
        res.status(500).send(e);
    }
});

router.get('/:id', async (req, res) => {
    try {
        const album = await Albums.findById(req.params.id);

        if (album) {
            return res.send(album);
        } else {
            return res.status(404).send({error: "Album not found"});
        }
    } catch (e) {
        res.status(500).send(e);
    }
});

router.post('/', upload.single('image'), async (req, res) => {
    try {
        if (!req.body.artist_id || !req.body.title || !req.body.productionYear) {
            return res.status(400).send({error: "Data is not valid"});
        }

        const albumData = {
            title: req.body.title,
            artist_id: req.body.artist_id,
            productionYear: req.body.productionYear
        }

        if (req.file) {
            albumData.image = req.file.filename;
        }

        const album = new Albums(albumData);
        await album.save();
        res.send(album);
    } catch (e) {
        res.status(500).send(e);
    }
});

module.exports = router;
