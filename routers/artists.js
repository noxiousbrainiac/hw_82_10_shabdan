const express = require('express');
const multer = require('multer');
const path = require('path');
const config = require('../config');
const Artists = require('../models/Artists');
const {nanoid} = require('nanoid');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const router = express.Router();

router.get('/', async (req, res) => {
    try {
        const artists = await Artists.find();
        res.send(artists);
    } catch (e) {
        res.status(500).send(e);
    }
});

router.post('/', upload.single('image'), async (req, res) => {
    try {
        if (!req.body.name) {
            return res.status(400).send({error: "Data is not valid"});
        }

        const artistData = {
            name: req.body.name,
        };

        if (req.file) {
            artistData.image = req.file.filename;
        }

        if (req.body.info) {
            artistData.info = req.body.info;
        }

        const artist = new Artists(artistData);
        await artist.save();
        res.send(artist);
    } catch (e) {
        res.status(500).send(e);
    }
});

module.exports = router;