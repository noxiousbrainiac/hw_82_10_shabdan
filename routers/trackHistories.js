const express = require('express');
const TrackHistory = require('../models/TrackHistory');
const User = require('../models/User');
const Tracks = require('../models/Tracks');
const mongoose = require('mongoose');

const router = express.Router();

router.post('/', async (req, res) => {
    try {
        if (!req.body.track) return res.status(401).send({error: "No track id"});

        const token = req.get('Authorization');

        if (!token) return res.status(401).send({error: "No token"});

        const user = await User.findOne({token});

        if (!user) return res.status(401).send({error: "Wrong token"});

        const track = await Tracks.findOne({_id: req.body.track});

        if (!track) return res.status(404).send({error: "No track with following ID"});

        const historyData = {
            track_id: track._id,
            user_id: user._id
        }

        const trackHistory = new TrackHistory(historyData);
        await trackHistory.save();
        res.send(trackHistory);
    } catch (e) {
        if(e instanceof mongoose.CastError) {
           return res.status(400).send({error: "Wrong track's id"});
        }
        res.status(500).send(e);
    }
});

module.exports = router;