const express = require('express');
const User = require('../models/User');
const bcrypt = require('bcrypt');

const router = express.Router();

router.post('/', async (req, res) => {
   try {
       if (!req.body.username || !req.body.password) {
           return res.status(400).send({error: "Invalid data"});
       }

       const userData = {
           username: req.body.username,
           password: req.body.password
       }

       const user = new User(userData);
       user.generateToken();
       await user.save();
       res.send(user);
   } catch (e) {
       res.status(500).send(e);
   }
});

router.post('/session', async (req, res) => {
   try {
       const user = await User.findOne({username: req.body.username});

       if (!user) return res.status(400).send({error: "Username not found"});

       const isMatch = await user.checkPassword(req.body.password);

       if (!isMatch) return res.status(401).send({error: "Wrong password"});

       user.generateToken();
       await user.save();
       res.send({message: "Username and password are correct!", user});
   } catch (e) {
       res.status(500).send(e);
   }
});

module.exports = router;