const express = require('express');
const Tracks = require('../models/Tracks');

const router = express.Router();

router.get('/', async (req, res) => {
    try {
        if (req.query.album) {
            const track = await Tracks.find({album_id: req.query.album});
            return res.send(track);
        }

        const tracks = await Tracks.find();
        res.send(tracks);
    } catch (e) {
        res.status(500).send(e);
    }
});

router.get('/:id', async (req, res) => {
   try {
       const track = await Tracks.findById(req.params.id);

       if (track) {
           return res.send(track);
       } else {
           return res.status(404).send({error: "Track not found"});
       }
   } catch (e) {
       res.status(500).send(e);
   }
});

router.post('/', async (req, res) => {
    try {
        if (!req.body.album_id || !req.body.duration || !req.body.title) {
            return res.status(400).send({error: "Data is not valid"});
        }

        const trackData = {
            title: req.body.title,
            album_id: req.body.album_id,
            duration: req.body.duration
        }

        const track = new Tracks(trackData);
        await track.save();
        res.send(track);
    } catch (e) {
        res.status(500).send(e);
    }
});

module.exports = router;